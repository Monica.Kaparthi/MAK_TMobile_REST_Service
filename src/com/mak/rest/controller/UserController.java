package com.mak.rest.controller;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.mak.rest.entity.UserBean;
import com.mak.rest.service.UserService;
import com.mak.rest.serviceImpl.UserServiceImpl;

@Path("/User")
public class UserController {
	private UserService userService = new UserServiceImpl();

	@GET
	@Path("/validate")
	//@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public String validateUserLoginDetails(@QueryParam("userName") String userName,
			@QueryParam("password") String password) {
		String validResult = "";
		System.out.println("In controller before get method");
		boolean valid = userService.validateUserLoginDetails(userName, password);
		System.out.println("In controller after return"+valid);
		if (valid) {
			validResult = "User is valid";
		} else {
			validResult = "User does not exist. Please enter correct username or password";
		}
		return validResult;
	}

	@POST
	@Path("/adduser")
	@Consumes(MediaType.APPLICATION_JSON)
	public String addUser(UserBean userBean) {
		System.out.println("In controller before method");
		String output = userService.addUserDetails(userBean);
		System.out.println("In controller after method return");
		return output;
	}
}
