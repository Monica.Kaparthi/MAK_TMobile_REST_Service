package com.mak.rest.service;

import com.mak.rest.entity.UserBean;

public interface UserService {

	//public boolean validateUserDetails(String userName, String password);
	
	public String addUserDetails(UserBean user);

	public boolean validateUserLoginDetails(String userName, String password);
}
